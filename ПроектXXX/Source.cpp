
#include<iostream>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
struct Point
{
	double x;
	double y;
	Point(double x1 = 0, double y1 = 0)
	{
		x = x1;
		y = y1;
	}
};
struct Line
{
	double A;
	double B;
	double C;
	Line(double A1 = 0, double B1 = 0, double C1 = 0)
	{
		A = A1;
		B = B1;
		C = C1;
	}
};
struct Point3
{
	double x3;
	double y3;
	double z3;
	Point3(double x31 = 0, double y31 = 0, double z31 = 0)
	{
		x3 = x31;
		y3 = y31;
		z3 = z31;
	}
};
struct Line3
{
	double a3;
	double b3;
	double c3;
	double x3;
	double y3;
	double z3;
	Line3(double a31 = 0, double b31 = 0, double c31 = 0, double x31 = 0, double y31 = 0, double z31 = 0)
	{
		a3 = a31;
		b3 = b31;
		c3 = c31;
		x3 = x31;
		y3 = y31;
		z3 = z31;
	}
};
struct Plane
{
	double A;
	double B;
	double C;
	double D;
	Plane(double A1 = 0, double B1 = 0, double C1 = 0, double D1 = 0)
	{
		A = A1;
		B = B1;
		C = C1;
		D = D1;
	}
};
double d;
double dlina;
double alfa;
double beta;
int I(Line l, Line l1, Point&p) // ������� ���������� ����� ����������� ������ �� ���������
{
	if ((l.A / l1.A) == (l.B / l1.B))
	{
		if ((l.C / l1.C) == (l.A / l1.A))
		{
			return 1;//"������ ���������"
		}
		else return 2; // " ������ �����������" 
	}
	else
	{
		p.x = (l.B*l1.C - l1.B*l.C) / (l.A*l1.B - l1.A*l.B);
		p.y = (l.C*l1.A - l1.C*l.A) / (l.A*l1.B - l1.A*l.B);
		return 3;
	}

} 

void II(Line l, Point p1)//������� ���������� ���������� �� ����� �� ������
{
	d= abs(l.A*p1.x + l.B*p1.y + l.C) / (sqrt(pow(l.A, 2) + pow(l.B, 2)));
}  

int III(Line3 l3, Line3 l31, Point3&p3)// ������� ���������� ����� ����������� ������ � ������������
{
	if ((l3.a3 / l31.a3) == (l3.b3 / l31.b3) && (l3.c3 / l31.c3) == (l3.a3 / l31.a3))    
	{
		double t = (l31.x3 - l3.x3) / l3.a3;
		if ((l31.x3 == (l3.x3 + l3.a3*t)) && (l31.y3 == (l3.y3 + l3.b3*t)) && (l31.z3 == (l3.z3 + l3.c3*t)))
		{
			return 1;
		}
		else
			return 2;
	}
	else
	{
		p3.x3 = (l3.x3*l3.b3*l31.a3 - l31.x3*l31.b3*l3.a3 - l3.y3*l3.a3*l31.a3 + l31.y3*l3.a3*l31.a3) / (l3.b3*l31.a3 - l31.b3*l3.a3);
		p3.y3 = (l3.y3*l3.a3*l31.b3 - l31.y3*l31.a3*l3.b3 - l3.x3*l3.b3*l31.b3 + l31.x3*l3.b3*l31.b3) / (l3.a3*l31.b3 - l31.a3*l3.b3);
		p3.z3 = (l3.z3*l3.b3*l31.c3 - l31.z3*l31.b3*l3.c3 - l3.y3*l3.c3*l31.c3 + l31.y3*l3.c3*l31.c3) / (l3.b3*l31.c3 - l31.b3*l3.c3);
		return 3;
	}
}

int IV(Plane plane, Plane plane1, Line3&l32)// ������� ���������� ����������� ���� ���������� 
{
	if ((plane.A / plane1.A) == (plane.B / plane1.B) && (plane.C / plane1.C) == (plane.A / plane1.A))
	{
		if ((plane.A / plane1.A) == (plane.D / plane1.D))
			return 1;
		else
			return 2;
	}
	else
	{
		l32.a3 = plane.B*plane1.C - plane.C*plane1.B;// ���� ������������ ������ 
		l32.b3 = plane.C*plane1.A - plane.A*plane1.C;
		l32.c3 = plane.A*plane1.B - plane.B*plane1.A;
		l32.x3 = 0;// ����������  �����, ������������� ������ ���������� 2-�� ����������
		l32.z3 = (plane.D*plane1.B - plane1.D*plane.B) / (plane1.C*plane.B - plane.C*plane1.B);
		l32.y3 = -(plane.C*l32.z3 + plane.D) / plane.B;
		return 3;
	}
}

int V(Line3 l3_5, Plane plane5,Point3&p5)// ������ ���������� ����� ���������� ������ � ��������� 
{
	if (((l3_5.a3*plane5.A) + (l3_5.b3*plane5.B) + (l3_5.c3*plane5.C)) == 0)
	{
		if (((l3_5.x3*plane5.A) + (l3_5.y3*plane5.B) + (l3_5.z3*plane5.C) + plane5.D) == 0)
			return 1;
		else
			return 2;
	}
	else
	{
		double t = -(plane5.A*l3_5.x3 + plane5.B*l3_5.y3 + plane5.C*l3_5.z3 + plane5.D) / (plane5.A*l3_5.a3 + plane5.B*l3_5.b3 + plane5.C*l3_5.c3);
		p5.x3 = l3_5.x3 + l3_5.a3*t;
		p5.y3 = l3_5.y3 + l3_5.b3*t;
		p5.z3 = l3_5.z3 + l3_5.c3*t;
		return 3;
	}
}

void VI(Plane plane6, Point3 p6)// ������� ���������� ���������� �� ����� �� ���������
{
	dlina = abs(plane6.A*p6.x3 + plane6.B*p6.y3 + plane6.C*p6.z3 + plane6.D) / (sqrt(plane6.A*plane6.A + plane6.B*plane6.B + plane6.C*plane6.C));
}

void VII(Line3 l3_7, Plane plane7)// ������� ���������� ���� ����� ������ � ����������
{
	alfa= asin((abs(l3_7.a3*plane7.A + l3_7.b3*plane7.B + l3_7.c3*plane7.C)) / (sqrt(l3_7.a3*l3_7.a3 + l3_7.b3*l3_7.b3 + l3_7.c3*l3_7.c3)*sqrt(plane7.A*plane7.A + plane7.B*plane7.B + plane7.C*plane7.C)));
}

void VIII(Line3 l3_8, Line3 l3_9)// ������� ���������� ���� ����� ������� � ������������
{
	beta = acos(abs(l3_8.a3*l3_9.a3 + l3_8.b3*l3_9.b3 + l3_8.c3*l3_9.c3) / (sqrt(l3_8.a3*l3_8.a3 + l3_8.b3*l3_8.b3 + l3_8.c3*l3_8.c3)*sqrt(l3_9.a3*l3_9.a3 + l3_9.b3*l3_9.b3 + l3_9.c3*l3_9.c3)));
}

 
void print()// 9) �����
{
	
}

template<typename T, typename ... Args>
void print(T val, Args... args)// args ����������
{
	cout << val << " ";
	print(args...);
}
int main()
{
	setlocale(LC_ALL, "russian");

	Point p;

	Point p1(3, 5);
	
	Line l(1, 5, -1);
	
	Line l1(3, 7, 5);
	
	Point3 p3;

	Line3 l3(3, 6, 9, 12, 21, -3);

	Line3 l31(3, 6, 9, 12, 21, -3);

	Line3 l32;
	
	Plane plane(3, 6, 8, 5);

	Plane plane1(65, -21, 13, -32);

	Plane plane5(3, -3, 2, -5);

	Line3 l3_5(2, 4, 3, -1, 3, 0);

	Point3 p5;
	
	Plane plane6(2, 4, -4, -6);
	
	Point3 p6(0, 3, 6);

	Plane plane7(1, 4, 5, 6);
	
	Line3 l3_7(1, 2, 3, 7, 5,2);
	
	Line3 l3_8(12, 3, 3, 7, 5, 3);

	Line3 l3_9(-2, 4, 4, 7, 9, 0);

	cout << "1)";
	switch (I(l, l1, p)) {
		
	case 1:
		if (I(l, l1, p) == 1)
			cout << "������ ���������" << endl;
	case 2:
		if (I(l, l1, p) == 2)
			cout << "������ �����������" << endl;
	case 3:
		if (I(l, l1, p) == 3) {
			cout << "����� ����������� ������:";
			print(p.x, p.y);
			cout << endl;
		}
	default:
			break;

	}//����� ������ �������
	
	II(l, p1);
	cout << "2)���������� �� ����� �� ������:  ";
	print(d);
	cout<< endl;//����� 2 �������
	cout << "3)";
	switch (III(l3, l31, p3)) 
	{
	case 1:
		if (III(l3, l31, p3) == 1)
			cout << "������ ���������" << endl;
	case 2:
		if (III(l3, l31, p3) == 2)
			cout << "������ �����������" << endl;
	case 3:
		if (III(l3, l31, p3) == 3) {
			cout << "����� ����������� ������ � ������������:";
			print(p3.x3, p3.y3, p3.z3);
			cout << endl;
		}
	default:
		break;
	}
	cout << "4)";
	switch (IV(plane, plane1, l32))
	{
	case 1:
		if (IV(plane, plane1, l32) == 1)
			cout << "��������� ���������" << endl;
	case 2:
		if (IV(plane, plane1, l32) == 2)
			cout << "��������� �����������" << endl;
	case 3:
		if (IV(plane, plane1, l32) == 3) {
			cout << "������ ����������� ���������� � ������������:";
			print(l32.a3, l32.b3, l32.c3, l32.x3,l32.y3,l32.z3);
			
		}
	default:
		break;
	}
	cout << "5)";
	switch (V(l3_5, plane5, p5))
	{
	case 1:
		if (V(l3_5, plane5, p5) == 1)
			cout << "������ ����������� ���������" << endl;
	case 2:
		if (V(l3_5, plane5, p5) == 2)
			cout << "������ � ���c����� �����������" << endl;
	case 3:
		if (V(l3_5, plane5, p5) == 3) 
		{
			cout << "������ ���������� ��������� � �����:";
			print(p5.x3,p5.y3,p5.z3);
			cout << endl;
		}
	default:
		break;
	}

	VI(plane6, p6);
	cout << "6)���������� �� ��������� �� ������:";
	print(dlina);
	cout << endl;//����� 6 �������
		
	VII(l3_7, plane7);
	cout << "7)���� ����� ������ � ����������:";
	print(alfa);
	cout << endl;//����� 7 �������

	VIII(l3_8, l3_9);
	cout << "8)���� ����� ������� � ������������:";
	print(beta);
	cout << endl;//����� 8 �������

	system("pause");
	return 0;
}


